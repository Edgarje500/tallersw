
#include <iostream>

using namespace std;
/*Description de la funcion:

Esta es la funcion main de nuestro
programa,en el tallersw*/

int main(){
	//Declaration of a variable
	int myFirstVariable;
	int SecondVariableInt;
	float myFirstFloat;
	double myFirstDouble;
	char myFirstChar;
	
	//Initialize
	myFirstVariable = 1;
	myFirstFloat = 2.54545;
	myFirstDouble = 2.999999999;
	myFirstChar = 64;
	
	//Display of the variable
	cout<<myFirstFloat;
	
	return 0;
}
