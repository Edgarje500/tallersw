/**
 * @file Template.cpp
 * @author Luis Arellano (luis.arellano09@gmail.com)
 * @brief File description:Exercice 1
 * @version 1.0
 * @date 28.11.2021
 * Elabore un algoritmo que dados como datos de entrada
  el radio y la altura de un cilindro calcular, 
  el �rea lateral y el volumen del cilindro.
 */
#include <iostream>
using namespace std;


int main(){
	
	//Declaration
	int radInput;
	int heightInput;
	float piFloat;
	float resultArea;
	float resultVolume;
	
	//Initialize
	radInput = 0;
	heightInput = 0;
	piFloat = 3.141592;
	resultVolume = 0;
	resultArea = 0;
	
	//Display phrase 1
	cout<<"Introduzca el radio:";
	cin>>radInput;
	cout<<"Introduzca la altura:";
	cin>>heightInput;
	
	//Operation
	resultArea = 2*piFloat*radInput*heightInput;
	resultVolume = piFloat*radInput*radInput*heightInput;
	
	//Display phrase 2
	cout<<"\r\nEl area es:"<<resultArea;
	cout<<"\r\nEl volumen es:"<<resultVolume;
	
	
	return 0;
}


