/**
 * @file CodeReviewExercise08.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 08:
 		Calcular el monto final, dados como datos el Capital Inicial, el  tipo de Inter�s,
 		el numero de periodos por a�o, y el numero de a�os de  la inversi�n.
  		El c�lculo del capital final se basa en la formula del inter�s compuesto.
		M = C(1+i/N)
	Donde:
	M = Capital final o Monto,	C = Capital Inicial,	i = Tipo de inter�s nominal
	N = Numero de periodos por a�o,		T = Numero de a�os		
 * @version 1.0
 * @date 11.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float capitalInitial = 0.0;	
float interestAnnual = 0.0;
int	  years = 0;
int	  periodsYear = 0;
int	  capitalizationInput=0;

float capitalEnd = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationCapitalEnd(float capital, float interest, int year, int capitalization);
int calculationPeriodsYear(int year);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the capital initial:";
	cin>>capitalInitial;
	cout<<"\tEnter the rate of interest annual:";
	cin>>interestAnnual;
	cout<<"\tEnter the number of years:";
	cin>>years;
	cout<<"\tEnter the number of capitalization(annual=1;bimonthly=6):";
	cin>>capitalizationInput;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	capitalEnd = calculationCapitalEnd(capitalInitial,interestAnnual,years,capitalizationInput);
	periodsYear = calculationPeriodsYear(years);	
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"The periods for year is:"<<periodsYear;
	cout<<"\tThe capital end is:"<<"$"<<capitalEnd<<"\r\n";
}
//=====================================================================================================

float calculationCapitalEnd(float capital, float interest, int year, int capitalization){
	return capital*pow((1 + interest/ ((float)year*365.0*capitalization)),capitalization);
}
//=====================================================================================================

int calculationPeriodsYear(int year){
	return year*365;
}
