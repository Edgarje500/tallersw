/**
 * @file CodeReviewExercise09.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 09:
 		�Cu�l es el capital que debe colocarse a inter�s  compuesto 
		 del 8%  anual para que despu�s de 20 a�os produzca un monto de $ 500,000. ? 	
 * @version 1.0
 * @date 15.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float capitalInitial = 0.0;	
float interestAnnual = 0.0;
int	  years = 0;

float capitalEnd = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationCapitalInitial(float capital, float interest, int year);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the capital end:";
	cin>>capitalEnd;
	cout<<"\tEnter the rate of interest annual:";
	cin>>interestAnnual;
	cout<<"\tEnter the number of years:";
	cin>>years;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	capitalInitial = calculationCapitalInitial(capitalEnd,interestAnnual,years);	
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe capital initial is:"<<"$"<<capitalInitial<<"\r\n";
}
//=====================================================================================================

float calculationCapitalInitial(float capital, float interest, int year){
	return capital/(pow((1+interest),year));
}
//=====================================================================================================

