/**
 * @file CodeReviewExercise17.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 17:
 		El �rea de una elipse se obtiene con la f�rmula ?ab , 
		donde a es el radio menor de la elipse y b es el radio mayor, y su per�metro se obtiene con la f�rmula ?
		[4(a+b)2]0.5. Realice un programa en C ++ utilizando estas f�rmulas y calcule el �rea
		y el per�metro de una elipse que tiene un radio menor de 2.5 cm y un radio mayor de 6.4 cm.
 * @version 1.0
 * @date 23.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416 //Constante Pi
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float radioMinorEllipse = 0.0;				
float radioHigherEllipse = 0.0;			

float areaEllipse = 0.0;					
float perimeterEllipse = 0.0;					

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationAreaEllipse(float radiusminor, float radiushigher);
float calculationPerimeterEllipse(float radiusminor, float radiushigher);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the radio minor of 2.5 cm:";
	cin>>radioMinorEllipse;
	cout<<"\tEnter the radio higher of 6.4 cm:";
	cin>>radioHigherEllipse;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	areaEllipse = calculationAreaEllipse(radioMinorEllipse,radioHigherEllipse);		
	perimeterEllipse= calculationPerimeterEllipse(radioMinorEllipse,radioHigherEllipse);	
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe area of the ellipse is: "<<areaEllipse<<"\r\n";
	cout<<"\tThe perimeter of the ellipse is: "<<perimeterEllipse<<"\r\n";
}
//=====================================================================================================

float calculationAreaEllipse(float radiusminor, float radiushigher){
	return radiusminor*radiushigher*PI;
}
//=====================================================================================================
float calculationPerimeterEllipse(float radiusminor, float radiushigher){
	return PI*4*pow((radiusminor+radiushigher),2)*0.5;
}

//=====================================================================================================

