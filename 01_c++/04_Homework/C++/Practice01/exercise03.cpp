/**
 * @file CodeReviewExecise03.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 03:
 		Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, necesitamos un algoritmo que lea el n�mero de desaprobados, aprobados, notables y sobresalientes de una asignatura, y nos devuelva:
		a. El tanto por ciento de alumnos que han superado la asignatura.
		b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.

 * @version 1.0
 * @date 11.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
 	int   totalStudent = 0;						//Total de estudiantes
 	int   desapprovedStudentInput = 0;			//Entrada de los estudiantes desaprobados
 	int   approvedStudentInput = 0;				//Entrada de los estudiantes aprobados
 	int   notableStudentInput = 0;				//Entrada de los estudiantes notables
 	int   outstandingStudentInput = 0;			//Entrada de los estudiantes sobresalientes
 	
 	float passedCursePorcentage = 0.0;			//Porcentaje de los que superaron el curso					
 	float desapprovedCursePorcentage = 0.0;		//Porcentaje de los que desaprobaron el curso
 	float approvedCursePorcentage = 0.0;		//Porcentaje de los que aprobaron el curso
 	float notableCursePorcentage = 0.0;			//Porcentaje de los notables en el curso
 	float outstandingCursePorcentage = 0.0;		//Porcentaje de los sobresalientes del curso

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int calculationTotalStudent(int desapproved, int approved, int notable, int outstanding);
float calculationPassedCursePorcentage(int total, int approved, int notable, int outstanding);
float calculationDesapprovedCursePorcentage(int total, int desapproved);
float calculationApprovedCursePorcentage(int total, int approved);
float calculationNotableCursePorcentage(int total, int notable);
float calculationOutstandingCursePorcentage(int total, int outstanding);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation in a course\r\n";
 	cout<<"\tEnter the number of desapproved:";
 	cin>>desapprovedStudentInput;
 	cout<<"\tEnter the number of approved:";
 	cin>>approvedStudentInput;
 	cout<<"\tEnter the number of notable:";
 	cin>>notableStudentInput;
 	cout<<"\tEnter the number of outstading:";
 	cin>>outstandingStudentInput;	
}
//=====================================================================================================

	//Operation of  the variables
void Calculate(){
	totalStudent = calculationTotalStudent(desapprovedStudentInput,approvedStudentInput,notableStudentInput,outstandingStudentInput); 
 	passedCursePorcentage = calculationPassedCursePorcentage(totalStudent,approvedStudentInput,notableStudentInput,outstandingStudentInput);
 	desapprovedCursePorcentage = calculationDesapprovedCursePorcentage(totalStudent,desapprovedStudentInput);
 	approvedCursePorcentage = calculationApprovedCursePorcentage(totalStudent,approvedStudentInput);
 	notableCursePorcentage = calculationNotableCursePorcentage(totalStudent,notableStudentInput);
 	outstandingCursePorcentage = calculationOutstandingCursePorcentage(totalStudent,outstandingStudentInput);
}
//=====================================================================================================

	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The total of student is:"<<totalStudent<<"\r\n";
 	cout<<"\tThe porcentage of those who passed the course are:"<<passedCursePorcentage<<"%\r\n";
 	cout<<"\tThe porcentage of those who desapproved the course are:"<<desapprovedCursePorcentage<<"%\r\n";
 	cout<<"\tThe porcentage of those who approved the course are:"<<approvedCursePorcentage<<"%\r\n";
 	cout<<"\tThe porcentage of those who notable the course are:"<<notableCursePorcentage<<"%\r\n";
 	cout<<"\tThe porcentage of those who outstanding the course are:"<<outstandingCursePorcentage<<"%\r\n";
 	
}
//=====================================================================================================

int calculationTotalStudent(int desapproved, int approved, int notable, int outstanding){
	return	desapproved + approved + notable + outstanding ;
}
//=====================================================================================================

float calculationPassedCursePorcentage(int total, int approved, int notable, int outstanding){
	return	((float)approved + (float)notable + (float)outstanding)*100.0/total;
	//Porcentaje de los que superaron el curso = ((aprovados + notables + sobresalientes) / total de estudiantes)*100
}
//=====================================================================================================

float calculationDesapprovedCursePorcentage(int total, int desapproved){
	return	((float)desapproved)*100.0/total;	
	//Porcentaje de los desaprobados = (desaprobados / total de estudiantes)*100
}
//=====================================================================================================

float calculationApprovedCursePorcentage(int total, int approved){
	return	((float)approved)*100.0/total;
	//Porcentaje de los aprobados = (aprobados / total de estudiantes)*100	
}
//=====================================================================================================

float calculationNotableCursePorcentage(int total, int notable){
	return	((float)notable)*100.0/total;
	//Porcentaje de los notables = (notables / total de estudiantes)*100	
}
//=====================================================================================================

float calculationOutstandingCursePorcentage(int total, int outstanding){
	return	((float)outstanding)*100.0/total;
	//Porcentaje de los sobresalientes = (sobresalientes / total de estudiantes)*100		
}
//=====================================================================================================

