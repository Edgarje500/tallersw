/**
 * @file CodeReviewExercise02.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 02:
 		Un maestro desea saber que porcentaje de hombres y que porcentaje de mujeres hay en un grupo de estudiantes. 
 * @version 1.0
 * @date 11.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int numberMenInput = 0;			//Entrada de numero de hombres
int numberWomenInput = 0;		//Entrada de numero de mujeres

float percentageWomen = 0.0;	//Porcentaje de mujeres
float percentageMen = 0.0;		//Porcentaje de varones

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationPercentageWomen(int women, int men);
float calculationPercentageMen(int men, int women);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();
	return 0;
	}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"============Insert data============\r\n";
	cout<<"Calculation of the percentage of men and women in a group student\r\n";
	cout<<"\tEnter the number of women:";
	cin>>numberWomenInput;
	cout<<"\tEnter the number of men:";
	cin>>numberMenInput;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	percentageWomen = calculationPercentageWomen(numberWomenInput,numberMenInput);
	percentageMen = calculationPercentageMen(numberWomenInput,numberMenInput);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe porcentage of women is:"<<percentageWomen<<"%\r\n";
	cout<<"\tThe porcentage of men is:"<<percentageMen<<"%\r\n";
}
//=====================================================================================================

float calculationPercentageWomen(int women, int men){	
	return ( (float)(women)/(float)(women + men) )*100.0 ;	//Porcentaje de mujeres = numero de mujeres / grupo estudiantil
}
//=====================================================================================================

float calculationPercentageMen(int men, int women){
	return ( (float)(men)/(float)(women + men) )*100.0;		//Porcentaje de varones = numero de hombres / grupo estudiantil	
}
//=====================================================================================================

