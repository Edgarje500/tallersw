/**
 * @file CodeReviewExercise12.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 12:
 		12.- Escriba un programa para calcular el tiempo transcurrido, en minutos, 
		 necesario para hacer un viaje. La ecuaci�n es tiempo transcurrido 
		 = distancia total/velocidad promedio. Suponga que la distancia est� en kil�metros y la velocidad en kil�metros/hora. 
 * @version 1.0
 * @date 11.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float distanceTotal = 0;
float speedAverage = 0;

int timeElapsedMinutes = 0.0;					//Tiempo transcurrido en minutos

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int calculationTimeElapsedMinutes(float distance, float speed);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter of the distance total in Kilometers:";
	cin>>distanceTotal;
	cout<<"\tEnter of the speed average in kilometers/hours:";
	cin>>speedAverage;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	timeElapsedMinutes = calculationTimeElapsedMinutes(distanceTotal,speedAverage);		
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe time elapsed in minutes is: "<<timeElapsedMinutes<<"\r\n";
}
//=====================================================================================================

int calculationTimeElapsedMinutes(float distance, float speed){
	return (distance / speed)*60.0;
}
//=====================================================================================================

