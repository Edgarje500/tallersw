/**
 * @file CodeReviewExecise04.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief Exercice 4:
  		4.- Un departamento de climatolog�a ha realizado recientemente su conversi�n al sistema m�trico.
		   Dise�ar un algoritmo para realizar las siguientes conversiones:
		a. Leer la temperatura dada en la escala Celsius e imprimir en su equivalente Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
		b. Leer la cantidad de agua en pulgadas e imprimir su equivalente en mil�metros (25.5 mm = 1pulgada.
 * @version 1.0
 * @date 11.12.2021
 *
 */
 /*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
	float celsiusTemperatureInput = 0.0;				//Entrada de temperatura en Celsius
	float inchesWaterInput = 0.0;						//Entrada de agua en pulgadas
	
	float fahrenheitWaterTemperatureWater = 0.0;		//Temperatura del agua en Fahrenheit
	float millimetersWaterAmount = 0.0;					//Cantidad de agua en milimetros

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationFahrenheitTemperature(float celsius);
float calculationMillimetersWater(float inches);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){

	Run();
	return 0;
}
  /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
 
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"============Insert data============\r\n";
	cout<<"Calculate the temperature in degrees Fahrenheit and the amount of water in milliliters\r\n";
	cout<<"\tEnter of the temperature in scale Celsius:";
	cin>>celsiusTemperatureInput;
	cout<<"\tEnter the amount of water in inches:";
	cin>>inchesWaterInput;	
}
//=====================================================================================================

	//Operation of  the variables
void Calculate(){
	fahrenheitWaterTemperatureWater = calculationFahrenheitTemperature(celsiusTemperatureInput); 
 	millimetersWaterAmount = calculationMillimetersWater(inchesWaterInput);
}
//=====================================================================================================

	//Show the calculation of the terminal
void ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe temperature in degrees Fahrenheit is:"<<fahrenheitWaterTemperatureWater<<"�F\r\n";
	cout<<"\tThe amount of water in milliliters:"<<millimetersWaterAmount<<"mm\r\n";
}
//=====================================================================================================

float calculationFahrenheitTemperature(float celsius){
	return	(celsius*1.8)+32;
}
//=====================================================================================================

float calculationMillimetersWater(float inches){
	return	inches / 25.5;
}
//=====================================================================================================

