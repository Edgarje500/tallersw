 /**
 * @file CodeReviewExercise11.cpp
 * @author Edgar Quiñones (edgar06022002@gmail.com)
 * @brief exercise 11:
 		Hacer un algoritmo para ingresar una medida en metros, y que imprima esa medida expresada en centímetros, 
		 pulgadas, pies y yardas. Los factores de conversión son los siguientes:
		1 yarda = 3 pies
		1 pie = 12 pulgadas
		1 metro = 100 centímetros
		1 pulgada = 2.54 centímetros
  
 * @version 1.0
 * @date 11.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float metersMeasureInput = 0.0;					//Entrada de la medida en metros

int centimeters = 0;						
int inches = 0;				
int feet = 0;	
int yard = 0;			

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int calculationCentimeters(float meters);
int calculationInches(float meters);
int calculationFeet(float meters);
int calculationYard(float meters);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter of the measure in meters:";
	cin>>metersMeasureInput;

}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	centimeters = calculationCentimeters(metersMeasureInput);		
	inches = calculationInches(metersMeasureInput);	
	feet = calculationFeet(metersMeasureInput);
	yard = calculationYard(metersMeasureInput);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe measure in centimeters is: "<<centimeters<<"\r\n";
	cout<<"\tThe measure in inches  is: "<<inches<<"\r\n";
	cout<<"\tThe measure in feet is: "<<feet<<"\r\n";
	cout<<"\tThe measure in yard  is: "<<yard<<"\r\n";
}
//=====================================================================================================

int calculationCentimeters(float meters){
	return meters * 100;
}
//=====================================================================================================

int calculationInches(float meters){
	return meters * (100.0/2.54);
}
//=====================================================================================================

int calculationFeet(float meters){
	return meters * (100.0/30.48);
}
//=====================================================================================================

int calculationYard(float meters){
	return meters * (100.0/91.44);
}
//=====================================================================================================
