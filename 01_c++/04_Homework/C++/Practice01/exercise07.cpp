/**
 * @file CodeReviewExercise07.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 07:
 		7.- Calcular la fuerza de atracci�n entre dos masas, separadas por una distancia, mediante la siguiente f�rmula:
		F = G*masa1*masa2 / distancia2
Donde G es la constante de gravitaci�n universal: G = 6.673 * 10-8 cm3/g.seg2
 * @version 1.0
 * @date 11.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define G  6.673 * pow(10,-8)//Constante de gravitaci�n universal
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float massFirstInput = 0.0;			//Entrada Primera masa
float massSecondInput = 0.0;		//Entrada Segunda masa
float distanceMasses = 0.0;			//Distancia entre las masas
float forceAttraction = 0.0;		//Fuerza de atracci�n

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationForceAttraction(float mass1, float mass2, float distance);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the first mass in grams:";
	cin>>massFirstInput;
	cout<<"\tEnter the second mass in grams:";
	cin>>massSecondInput;
	cout<<"\tEnter the distance between masses in centimeters:";
	cin>>distanceMasses;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	forceAttraction =calculationForceAttraction(massFirstInput,massSecondInput,distanceMasses);			
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe force of attraction between the masses is:"<<forceAttraction<<"g*cm/s2\r\n";
}
//=====================================================================================================

float calculationForceAttraction(float mass1, float mass2, float distance){
	return (G*mass1*mass2)/pow(distance,2.0);
}
//=====================================================================================================

