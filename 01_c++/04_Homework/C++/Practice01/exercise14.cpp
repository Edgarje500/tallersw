/**
 * @file CodeReviewExercise14.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 14:
  	Modifique el programa anterior para que, suponiendo que las medidas de entrada son dadas en cent�metros,
  	el resultado lo muestre en: litros, cent�metros c�bicos y metros c�bicos. 
  	Recuerde que 1 litro es equivalente a un dec�metro c�bico. Indique siempre la unidad de medida empleada.
 * @version 1.0
 * @date 11.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416 //Constante Pi
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float lengthBarrel = 0.0;			//Longitud  de un tonel
float heightBarrel = 0.0;			//Altura de un tonel
float diameterBarrelEnds = 0.0;		//Diametro a los extremos de un tonel
float diameterBarrelCenter = 0.0;	//Diametro a los extremosde un tonel

float volumeBarrel1 = 0.0;			//volumen de un tonel en litros
float volumeBarrel2 = 0.0;			//volumen de un tonel en centimetros cubicos
float volumeBarrel3 = 0.0;			//volumen de un tonel en metros cubicos
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationHeightBarrel(float diameter1, float diameter2);
float calculationVolumeBarrel1(float length, float altura);
float calculationVolumeBarrel2(float length, float altura);
float calculationVolumeBarrel3(float length, float altura);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the length of the barrel in centimeter:";
	cin>>lengthBarrel;
	cout<<"\tEnter the diameter in the ends of the barrel in centimeter:";
	cin>>diameterBarrelEnds;
	cout<<"\tEnter the diameter in the center of the barrel in centimeter:";
	cin>>diameterBarrelCenter;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	heightBarrel = calculationHeightBarrel(diameterBarrelEnds,diameterBarrelCenter);
	volumeBarrel1 = calculationVolumeBarrel1(lengthBarrel,heightBarrel);
	volumeBarrel2 = calculationVolumeBarrel2(lengthBarrel,heightBarrel);
	volumeBarrel3 = calculationVolumeBarrel3(lengthBarrel,heightBarrel);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe volume of the barrel in liters is: "<<volumeBarrel1<<"L\r\n";
	cout<<"\tThe volume of the barrel in centimeter cubic is: "<<volumeBarrel2<<"cm3\r\n";
	cout<<"\tThe volume of the barrel in meters cubic is: "<<volumeBarrel3<<"m3\r\n";
}
//=====================================================================================================

float calculationHeightBarrel(float diameter1, float diameter2){
	return (diameter1/2) + 0.66*((diameter2/2) - (diameter1/2));
}	
//=====================================================================================================

float calculationVolumeBarrel1(float length, float altura){
	return (PI*length*pow(altura,2))/1000;
}
//=====================================================================================================

float calculationVolumeBarrel2(float length, float altura){
	return PI*length*pow(altura,2);
}
//=====================================================================================================

float calculationVolumeBarrel3(float length, float altura){
	return (PI*length*pow(altura,2))/100000;
}
//=====================================================================================================

