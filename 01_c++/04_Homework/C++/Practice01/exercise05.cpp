/**
 * @file CodeReviewExercise05.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 05:
 		El costo de un autom�vil nuevo para un comprador es la suma total del costo del veh�culo,
 		del porcentaje de la ganancia del vendedor y de los impuestos locales o estatales aplicables
 		 (sobre el precio de venta). 
 		 Suponer una ganancia del vendedor del 12% en todas las unidades y un impuesto del 6%
  		 y dise�ar un algoritmo para leer el costo total del autom�vil e imprimir el costo para el consumidor.		
 * @version 1.0
 * @date 11.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float costTotalCar = 0.0;		//Costo total del autom�vil

float costConsumer = 0.0;		//Costo para el consumidor

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationCostConsumer(float costtotal);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the cost total of the car:";
	cin>>costTotalCar;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	costConsumer = calculationCostConsumer(costTotalCar);		
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe cost for the condumer is: "<<costConsumer<<"\r\n";
}
//=====================================================================================================

float calculationCostConsumer(float costtotal){
	return costtotal + costtotal*0.12 + (costtotal*(1.12))*(0.06);
}
//=====================================================================================================
