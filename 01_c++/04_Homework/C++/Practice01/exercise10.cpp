/**
 * @file CodeReviewExercise10.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 10:
 		Un millonario exc�ntrico ten�a tres hijos: Carlos, Jos� y Marta.
		  Al morir dej� el siguiente legado: A Jos� le dej� 4/3 de lo que le dej� a Carlos.
		   A Carlos le dej� 1/3 de su fortuna. A Marta le dejo la mitad de lo que le dej� a Jos�.
		    Preparar un algoritmo para darle la suma a repartir e imprima cuanto le toc� a cada uno. 	
 * @version 1.0
 * @date 15.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float amountMoney = 0.0;		//Suma de dinero	

float sonCarlos = 0.0;
float sonJose = 0.0;
float daughterMarta = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationSonCarlos(float money);
float calculationSonJose(float money);
float calculationDaughterMarta(float money);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the amount of money:";
	cin>>amountMoney;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	sonCarlos = calculationSonCarlos(amountMoney);	
	sonJose = calculationSonJose(sonCarlos);
	daughterMarta = calculationDaughterMarta(sonJose);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe money what's up to Carlos is:"<<"$"<<sonCarlos<<"\r\n";
	cout<<"\tThe money what's up to Jose is:"<<"$"<<sonJose<<"\r\n";
	cout<<"\tThe money what's up to Marta is:"<<"$"<<daughterMarta<<"\r\n";
}
//=====================================================================================================

float calculationSonCarlos(float money){
	return money/3;
}
//=====================================================================================================

float calculationSonJose(float carlos){
	return (carlos*4)/3;
}
//=====================================================================================================

float calculationDaughterMarta(float jose){
	return jose/2;
}

