/**
 * @file CodeReviewExercise18.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 18:
 		Si coloca una escalera de 3 metros a un �ngulo de 85 grados al lado de un edificio,
		la altura en la cual la escalera toca el edificio se puede calcular como altura=3 * seno 85�. 
		Calcule esta altura con una calculadora y luego escriba un programa en C que obtenga y visualice el valor de la altura. 
		Nota: Los argumentos de todas las funciones trigonom�tricas (seno, coseno, etc) deben estar expresados en radianes.
		Por tanto, para obtener el seno, por ejemplo, de un �ngulo expresado en grados, primero deber� convertir el �ngulo a radianes.
 * @version 1.0
 * @date 23.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416 //Constante Pi
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int degreesSexagesimal = 0;				

float heightStairs = 0.0;					

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationHeightStairs(int degrees);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"Calculation of the height at which the ladder touches the building\r\n";
	cout<<"\tEnter the degrees sexagesimal:";
	cin>>degreesSexagesimal;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	heightStairs = calculationHeightStairs(degreesSexagesimal);			
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe height at which the ladder touches the building is: "<<heightStairs<<"\r\n";
}
//=====================================================================================================

float calculationHeightStairs(int degrees){
	return 3*sin((float)degrees*0.0174532925);		
}

