/**
 * @file CodeReviewExercise13.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 13:
 		Un tonel es un recipiente, generalmente de madera, muy utilizado para almacenar y mejorar un vino.
		La forma de un tonel es muy caracter�stica y es un cilindro en el que la parte central es m�s gruesa, es decir,
		tiene un di�metro mayor que los extremos. Escriba un programa que lea las medidas de un tonel y nos devuelva su
		capacidad, teniendo en cuenta que el volumen (V) de un tonel viene dado por la siguiente f�rmula: V = pi l a2 donde:
 		l  es la longitud del tonel, su altura. a = d/2 + 2/3(D/2 - d/2)
		d  es el di�metro del tonel en sus extremos.
		D  es el di�metro del tonel en el centro: D>d
		Nota: Observe que si las medidas se dan en cent�metros el resultado lo obtenemos en cent�metros c�bicos.
 * @version 1.0
 * @date 11.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416 //Constante Pi
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float lengthBarrel = 0.0;			//Longitud  de un tonel
float heightBarrel = 0.0;			//Altura de un tonel
float diameterBarrelEnds = 0.0;		//Diametro a los extremos de un tonel
float diameterBarrelCenter = 0.0;	//Diametro a los extremosde un tonel

float volumeBarrel = 0.0;			//volumen de un tonel
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationHeightBarrel(float diameter1, float diameter2);
float calculationVolumeBarrel(float length, float altura);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the length of the barrel in centimeter:";
	cin>>lengthBarrel;
	cout<<"\tEnter the diameter in the ends of the barrel in centimeter:";
	cin>>diameterBarrelEnds;
	cout<<"\tEnter the diameter in the center of the barrel in centimeter:";
	cin>>diameterBarrelCenter;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	heightBarrel = calculationHeightBarrel(diameterBarrelEnds,diameterBarrelCenter);
	volumeBarrel = calculationVolumeBarrel(lengthBarrel,heightBarrel);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe volume of the barrel is: "<<volumeBarrel<<"cm3\r\n";
}
//=====================================================================================================

float calculationHeightBarrel(float diameter1, float diameter2){
	return (diameter1/2) + 0.66*((diameter2/2) - (diameter1/2));
}	
//=====================================================================================================

float calculationVolumeBarrel(float length, float altura){
	return PI*length*pow(altura,2);
}
//=====================================================================================================


