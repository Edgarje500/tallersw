/**
 * @file CodeReviewExercise16.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 16:
 		16. Escribe, compila y ejecuta un programa en C ++ que calcule y devuelva la ra�z cuarta de un n�mero.
		Pru�balo con el n�mero 81.0 (deber� devolverte 3).
		Utiliza el programa para calcular la ra�z cuarta de 1728.8964
 * @version 1.0
 * @date 18.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float numberInput = 0.0;					//Entrada del n�mero

float numberFourthRootCalculate = 0.0;				//Calculo de la raiz cuarta del n�mero

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationNumberFourthRootCalculate(float number);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the number:";
	cin>>numberInput;

}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	numberFourthRootCalculate = calculationNumberFourthRootCalculate(numberInput);			
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe root fourth of the number is: "<<numberFourthRootCalculate<<"\r\n";
}
//=====================================================================================================

float calculationNumberFourthRootCalculate(float number){
	return pow(number,(0.25));
}








