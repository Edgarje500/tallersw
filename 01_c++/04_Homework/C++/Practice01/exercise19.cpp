/**
 * @file CodeReviewExercise19.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
* @brief exercise 19:
 		Dados como datos las coordenadas de los tres puntos P1, P2, P3 que corresponden a los v�rtices de un triangulo,
		calcule su per�metro y �rea.
 * @version 1.0
 * @date 23.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <cstdlib>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int pointX1 = 0;					
int pointY1 = 0;			
int pointX2 = 0;				
int pointY2 = 0;
int pointX3 = 0;				
int pointY3 = 0;				

float perimeterTriangle = 0.0;
float areaTriangle = 0.0;		
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationPerimeterTriangle(int X1, int X2, int X3, int Y1, int Y2, int Y3);
float calculationAreaTriangle(int X1, int X2, int X3, int Y1, int Y2, int Y3);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the point X1:";
	cin>>pointX1;
	cout<<"\tEnter the point Y1:";
	cin>>pointY1;
	cout<<"\tEnter the point X2:";
	cin>>pointX2;
	cout<<"\tEnter the point Y2:";
	cin>>pointY2;
	cout<<"\tEnter the point X3:";
	cin>>pointX3;
	cout<<"\tEnter the point Y3:";
	cin>>pointY3;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	perimeterTriangle = calculationPerimeterTriangle(pointX1,pointX2,pointX3,pointY1,pointY2,pointY3);		
	areaTriangle = calculationAreaTriangle(pointX1,pointX2,pointX3,pointY1,pointY2,pointY3);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe perimeter of the triangle is: "<<perimeterTriangle<<"\r\n";
	cout<<"\tThe area of the triangule is: "<<areaTriangle<<"\r\n";
}
//=====================================================================================================

float calculationPerimeterTriangle(int X1, int X2, int X3, int Y1, int Y2, int Y3){
	return pow(pow((X2-X1),2),0.5) + pow(pow((X3-X2),2),0.5) +pow(pow((X3-X1),2),0.5);		
}
//=====================================================================================================

float calculationAreaTriangle(int X1, int X2, int X3, int Y1, int Y2, int Y3){
	return abs( (X1*(Y2-Y3))+(X2*(Y3-Y1))+(X3*(Y1-Y2) ) )/2;
}
//=====================================================================================================
