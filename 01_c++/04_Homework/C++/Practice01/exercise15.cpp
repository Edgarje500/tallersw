/**
 * @file CodeReviewExercise15.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 15:
 		Escriba un programa en C para calcular el valor de la pendiente de una l�nea que conecta dos puntos (x1,y1) y (x2,y2).
		La pendiente est� dada por la ecuaci�n (y2-y1)/(x2-x1). 
		Haga que el programa tambi�n calcule el punto medio de la l�nea que une los dos puntos, 
		el cual viene dado por (x1+x2)/2,(y1+y2)/2. �Cu�l es el resultado que devuelve el programa para los puntos (3,7) y (8,12)? 
 * @version 1.0
 * @date 23.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int pointX1 = 0;					
int pointY1 = 0;			
int pointX2 = 0;				
int pointY2 = 0;				

float earringStraight = 0.0;					
int pointMediumX = 0;					
int pointMediumY = 0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationEarringStraight(int X1, int X2, int Y1, int Y2);
int calculationPointMediumX(int X1, int X2);
int calculationPointMediumY(int Y1, int Y2);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the point X1:";
	cin>>pointX1;
	cout<<"\tEnter the point Y1:";
	cin>>pointY1;
	cout<<"\tEnter the point X2:";
	cin>>pointX2;
	cout<<"\tEnter the point Y2:";
	cin>>pointY2;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	earringStraight = calculationEarringStraight(pointX1,pointX2,pointY1,pointY2);		
	pointMediumX = calculationPointMediumX(pointX1,pointX2);
	pointMediumY = calculationPointMediumY(pointY1,pointY2);	
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe lateral area of the cylinder is: "<<earringStraight<<"\r\n";
	cout<<"\tThe volume of the cylinder is: "<<"("<<pointMediumX<<","<<pointMediumY<<")"<<"\r\n";
}
//=====================================================================================================

float calculationEarringStraight(int X1, int X2, int Y1, int Y2){
	return (Y2 - Y1)/(X2 - X1);		
}
//=====================================================================================================
int calculationPointMediumX(int X1, int X2){
	return (X1 + X2)/2;
}

//=====================================================================================================
int calculationPointMediumY(int Y1, int Y2){
	return (Y1 + Y2)/2;
}

//=====================================================================================================
