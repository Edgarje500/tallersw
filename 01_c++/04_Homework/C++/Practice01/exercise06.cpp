/**
 * @file codeReviewExercise06.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief File description exercice 06:
 		Desglosar cierta cantidad de segundos a su equivalente en d�as, horas, minutos y segundos
 * @version 1.0
 * @date 11.12.2021
 * 
 */
 /*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
 #include <iostream>
 using namespace std;
 /*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int totalSecondsInput = 0;	//Entrada del total de segundos

int days = 0;				
int hours = 0;				
int minutes = 0;
int seconds = 0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int calculationDays(int total);
int calculationHours(int total);
int calculationMinutes(int total);
int calculationSeconds(int total);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
 	
	Run();
 	return 0;	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
 	cout<<"Calculate the number total of seconds in days ,hours ,minutes and seconds\r\n";
 	cout<<"\tEnter the number of total seconds:";
 	cin>>totalSecondsInput;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	days = calculationDays(totalSecondsInput);
	hours = calculationHours(totalSecondsInput);
	minutes = calculationMinutes(totalSecondsInput);
	seconds = calculationSeconds(totalSecondsInput);
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
 	cout<<"\tThe number of days is:"<<days<<"\r\n";
 	cout<<"\tThe number of hours is:"<<hours<<"\r\n";
 	cout<<"\tThe number of minutes is:"<<minutes<<"\r\n";
 	cout<<"\tThe number of seconds is:"<<seconds<<"\r\n";
}
//=====================================================================================================

int calculationDays(int total){	
	return total / 86400;
}
//=====================================================================================================

int calculationHours(int total){
	total = total % 86400;
	return	total / 3600;		
}
//=====================================================================================================

int calculationMinutes(int total){
	total = total % 3600;
	return	total / 60;
}
//=====================================================================================================

int calculationSeconds(int total){
	return total % 60;
}
//=====================================================================================================
