/**
 * @file CodeReviewExercise01.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 01:
 		Elabore un algoritmo que dados como datos  de entrada el radio y la altura de un cilindro calcular,
	  	el �rea lateral y el volumen del cilindro.
     	A = 2   radio*altura	V =   radio2*altura
 * @version 1.0
 * @date 11.12.2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416 //Constante Pi
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float radioCylinderInput = 0.0;					//Entrada del radio del Cilindro
float heightCylinderInput = 0.0;				//Entrada de la altura del Cilindro

float cylindertAreaSide = 0.0;					//Calculo del Area Lateral del Cilindro
float cylindertVolume = 0.0;					//Calculo del Volumen del Cilindro

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationCylindertAreaSide(float radius, float height);
float calculationCylindertVolume(float radius, float height);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEnter the radio:";
	cin>>radioCylinderInput;
	cout<<"\tEnter the height:";
	cin>>heightCylinderInput;
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	cylindertAreaSide = calculationCylindertAreaSide(radioCylinderInput,heightCylinderInput);		
	cylindertVolume = calculationCylindertVolume(radioCylinderInput,heightCylinderInput);	
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe lateral area of the cylinder is: "<<cylindertAreaSide<<"\r\n";
	cout<<"\tThe volume of the cylinder is: "<<cylindertVolume<<"\r\n";
}
//=====================================================================================================

float calculationCylindertAreaSide(float radius, float height){
	return 2.0 * PI * radius * height;		// area lateral = 2 * pi * radio * h
}
//=====================================================================================================
float calculationCylindertVolume(float radius, float height){
	return PI * pow(radius,2.0) * height;	//volumen=PI*radio*radio*altura
}

//=====================================================================================================









