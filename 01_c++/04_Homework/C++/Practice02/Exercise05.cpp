/**
* @file CondicionalesExercise07.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 05:
 		5.- Dados tres datos enteros positivos, que representen las longitudes de un posible triangulo, determine si los datos corresponden a un triangulo. En caso afirmativo, escriba si el triangulo es equil�tero, is�sceles o escaleno. Calcule adem�s su �rea.
 * @version 1.0
 * @date 09.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float sideTriangleOne = 0.0;
float sideTriangleTwo = 0.0;
float sideTriangleThree = 0.0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool ShowResults();
bool CheckEquilateral(float side1, float side2, float side3);
bool CheckIsosceles(float side1, float side2, float side3);
bool CheckScalene(float side1, float side2, float side3);
float CalculatedTriangle(float side1, float side2, float side3, float sum);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
	
		break;
	}
			
	return true;
}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter the first side: ";
	cin>>sideTriangleOne;
	
	cout<<"Enter the two side: ";
	cin>>sideTriangleTwo;
	
	cout<<"Enter the third side:";
	cin>>sideTriangleThree;

 	if (sideTriangleOne <= 0){
		cout<<"Side Triangle not valid\r\n";
		return Error;
	}
	
	if (sideTriangleTwo <= 0){
		cout<<"Side Triangle not valid\r\n";
		return Error;
	}
	
	if (sideTriangleThree <= 0){
		cout<<"Side Triangle not valid\r\n";
		return Error;
	}
	
		return Success;
}
//=====================================================================================================

bool ShowResults(){
	float areaTriangle = 0.0;
	float semiSum = 0.0;
	
	if (areaTriangle < 0){
		cout<<"Area triangle not valid\r\n";
		return false;
	}
	
	if (semiSum < 0){
		cout<<"Semi Sum invalid\r\n";
		return false;	
	}
	
	if (CheckEquilateral(sideTriangleOne,sideTriangleTwo,sideTriangleThree)){
		cout<<"the triangle is equilateral\r\n";
 	}else	if (CheckIsosceles(sideTriangleOne,sideTriangleTwo,sideTriangleThree)){
 		cout<<"the triangle is isosceles\r\n";
	}else	if (CheckScalene(sideTriangleOne,sideTriangleTwo,sideTriangleThree)){
	 	cout<<"the triangle is scalene\r\n";
	}	
	areaTriangle=CalculatedTriangle(sideTriangleOne,sideTriangleTwo,sideTriangleThree,semiSum);
	cout<<"The area of the triangle is:"<<areaTriangle<<"\r\n";	
	return true;
}
//=====================================================================================================

bool CheckEquilateral(float side1, float side2, float side3){
	if ( (side1 == side2) && (side1 == side3) && (side2== side3)) {
		return true;	
	}else{
		return false;
	}
}
//=====================================================================================================

bool CheckIsosceles(float side1, float side2, float side3){
	if ( (side1 == side2) || (side1 == side3) || (side2 == side3)) {
		return true;
	}else{
		return false;
	}
}
//=====================================================================================================

bool CheckScalene(float side1, float side2, float side3){
	if (side1 != side2 != side3) {
		return true;	
   	}
}
//=====================================================================================================

float CalculatedTriangle(float side1, float side2, float side3, float sum){
	sum = (side1 + side2 + side3)/2;
		return pow((sum*(sum-side1)*(sum-side2)*(sum-side3)),0.5);		
	}
//=====================================================================================================

