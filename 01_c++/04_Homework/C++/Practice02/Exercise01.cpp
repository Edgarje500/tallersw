/**
 * @file CondicionalesExercise01.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 01:
 		1.- Calcular el pago semanal de un trabajador. Los datos a ingresar son: Total de horas trabajadas y el pago por hora.
		Si el total de horas trabajadas es mayor a 40 la diferencia se considera como horas extras y se paga un 50% mas que una hora normal.
		Si el sueldo bruto es mayor a s/. 500.00, se descuenta un 10% en caso contrario el descuento es 0.
 * @version 1.0
 * @date 04.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalParamLimitedHours = 40;
int globalParamLimitedSalary = 500;

float globalParamPercentageIncreasedSalary = 1.5;
float globalParamPercentageDiscountSalary = 0.9;

int totalHoursWorked = 0;
float payHours = 0.0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool ShowResults();
float CalculatedGrossSalary(int hours, float pay);
float CalculatedPayWeekly(float salary);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
		while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
		
		break;
	}		
	return true;
}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter number of hours worked: ";
	cin>>totalHoursWorked;
	
	cout<<"Enter the pay for hours: ";
	cin>>payHours;
	
	if((totalHoursWorked < 0)){
		cout<<"Error total hours worked is not valid\r\n";
		return Error;
	}

	if((payHours < 0)){
		cout<<"Error pay hours is not valid\r\n";
		return Error;
	}
	return Success;	
}

//=====================================================================================================

bool ShowResults(){
	float grossSalary = 0.0;
	float payWeekly = 0.0;
	
	if (grossSalary < 0){
		cout<<"Gross Salary not valid\r\n";
		return false;
	}
	
	if (payWeekly < 0){
		cout<<"Pay Weekly invalid\r\n";
		return false;	
	}

	cout<<"============Show result============\r\n";
	if ( grossSalary=CalculatedGrossSalary(totalHoursWorked,payHours) ) {
		cout<<"The gross salary is:"<<grossSalary<<"\r\n";
		if ( payWeekly=CalculatedPayWeekly(grossSalary) ) {
		cout<<"The final salary is:"<<payWeekly<<"\r\n";
		} else {
		cout<<"The final salary is:"<<payWeekly<<"\r\n";
		}
	} else {
		cout<<"The gross salary is:"<<grossSalary<<"\r\n";
	}	
	return true;
}
//=====================================================================================================

float CalculatedGrossSalary(int hours, float pay){
	if ( hours > globalParamLimitedHours) {
		return pay*(((hours-globalParamLimitedHours)*globalParamPercentageIncreasedSalary)+globalParamLimitedHours);
	} else {
		return pay*hours;
	}
}
//=====================================================================================================

float CalculatedPayWeekly(float salary){
	if ( salary > globalParamLimitedSalary) {
		return salary*(globalParamPercentageDiscountSalary);	
	} else {
		return salary;	
	}
}
//=====================================================================================================

