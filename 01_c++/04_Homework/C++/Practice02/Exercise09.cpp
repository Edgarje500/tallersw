/**
 * @file CondicionalesExercise09.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 09:
 		9.- Calcular la comisi�n sobre las ventas totales de un empleado, sabiendo que el empleado no recibe comisi�n si su venta es hasta S/.150, si la venta es superior a S/.150 y menor o igual a S/.400 el empleado recibe una comisi�n del 10% de las ventas y si las ventas son mayores a 400, entonces la comisi�n es de S/.50 m�s el 9% de las ventas.
 * @version 1.0
 * @date 08.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalParamRangeSale1 = 150;
int globalParamRangeSale2 = 400;
float saleInput = 0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool ShowResults();
bool CheckSale(float sale);
float CalculatedCommission(float sale);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
		while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
		
		break;
	}		
	return true;
}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter you sale: ";
	cin>>saleInput;

	if((saleInput < 0)){
		cout<<"Sale is not valid\r\n";
		return Error;
	}
	return Success;	
}
//=====================================================================================================

bool ShowResults(){
	float commission = 0;
	
	if (commission < 0){
		cout<<"Commission not valid\r\n";
		return false;
	}
	
	if (CheckSale(saleInput) ) {
		cout<<"you have no commission"<<"\r\n";
	}else	if(commission=CalculatedCommission(saleInput)){
		cout<<"you commission is:"<<commission<<"\r\n";
	}	return true;
}
//=====================================================================================================
bool CheckSale(float sale){
	if ( globalParamRangeSale1 >= sale) {
		return true;
	} else{
		return false;
	}
}
//=====================================================================================================

float CalculatedCommission(float sale){		
	if ( globalParamRangeSale1 < sale && sale <= globalParamRangeSale2) {
		return (0.1)*sale;
	}else	if ( globalParamRangeSale2 < sale) {
		return sale*(0.09)+50;
	}
}

