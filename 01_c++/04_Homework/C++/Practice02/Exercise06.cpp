/**
 * @file CondicionalesExercise06.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 06:
 		6.- Dada la hora del d�a en horas, minutos y segundos encuentre la hora del siguiente segundo.
 * @version 1.0
 * @date 07.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
 /*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int hoursInput = 0;				
int minutesInput = 0;				
int secondsInput = 0;

int totalSeconds = 0;

int hoursExit = 0;				
int minutesExit = 0;				
int secondsExit = 0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
bool Calculate();
bool ShowResults();
int CalculationHours(int hours, int minutes, int seconds, int total);
int CalculationMinutes(int hours, int minutes, int seconds, int total);
int CalculationSeconds(int hours, int minutes, int seconds, int total);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
 	
	Run();
 	return 0;	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (Calculate() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
	
		break;
	}
			
	return true;
}

//=====================================================================================================

	//Data entry in the terminal
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
 	cout<<"Calculate the hours of the next seconds\r\n";
 	cout<<"\tEnter the number of hours:";
 	cin>>hoursInput;
 	cout<<"\tEnter the number of minutes:";
 	cin>>minutesInput;
 	cout<<"\tEnter the number of seconds:";
 	cin>>secondsInput;
 	
 	if ((hoursInput < 0) || (hoursInput > 24)){
		cout<<"Hours not valid\r\n";
		return Error;
	}
	
	if ((minutesInput < 0) || (minutesInput > 59)){
		cout<<"Minutes not valid\r\n";
		return Error;
	}
	
	if ((secondsInput < 0) || (secondsInput > 59)){
		cout<<"Total seconds not valid\r\n";
		return Error;
	}
		return Success;
}
//=====================================================================================================
	//Operation of  the variables
bool Calculate(){
	
	minutesExit = CalculationMinutes(hoursInput,minutesInput,secondsInput,totalSeconds);
	secondsExit = CalculationSeconds(hoursInput,minutesInput,secondsInput,totalSeconds);
	
	if (hoursExit < 0){
		cout<<"Hours not valid\r\n";
		return false;
	}
	
	if (minutesExit < 0){
		cout<<"Minutes invalid\r\n";
		return false;
	}
	
	if (secondsExit < 0){
		cout<<"The seconds is invalid\r\n";
		return false;
	}
		return true;
	
}
//=====================================================================================================
	//Show the calculation of the terminal
bool ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	if (hoursExit = CalculationHours(hoursInput,minutesInput,secondsInput,totalSeconds)){
		cout<<"\tThe number of hours of the next seconds is:"<<hoursExit<<"\r\n";
		cout<<"\tThe number of minutes of the next seconds is:"<<minutesExit<<"\r\n";
 		cout<<"\tThe number of seconds of the next seconds is:"<<secondsExit<<"\r\n";
	}else {
		cout<<"\tThe number of hours of the next seconds is:"<<hoursExit<<"\r\n";
		cout<<"\tThe number of minutes of the next seconds is:"<<minutesExit<<"\r\n";
 		cout<<"\tThe number of seconds of the next seconds is:"<<secondsExit<<"\r\n";
	}
 	return true;
}
//=====================================================================================================

int CalculationHours(int hours, int minutes, int seconds, int total){
	if ( (hours==24) && (minutes==59) && (seconds==59)){
		return 1;	
	}else{
		total = hours*3600 + minutes*60 + seconds + 1;
		return	total / 3600;
	}		
}
//=====================================================================================================

int CalculationMinutes(int hours, int minutes, int seconds, int total){
	total = hours*3600 + minutes*60 + seconds + 1;
	total = total % 3600;
	return	total / 60;
}
//=====================================================================================================

int CalculationSeconds(int hours, int minutes, int seconds, int total){
	total = hours*3600 + minutes*60 + seconds + 1;
	return total % 60;
}
//=====================================================================================================


