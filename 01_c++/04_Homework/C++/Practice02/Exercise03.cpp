/**
 * @file CondicionalesExercise03.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 03:
 		3.- Ordene de mayor a menor 3 n�meros  ingresados por teclado
 * @version 1.0
 * @date 09.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int variableOne = 0;
int variableTwo = 0;
int variableThree = 0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool ShowResults();
bool CheckOrder1(int variable1, int variable2, int variable3);
bool CheckOrder2(int variable1, int variable2, int variable3);
bool CheckOrder3(int variable1, int variable2, int variable3);
bool CheckOrder4(int variable1, int variable2, int variable3);
bool CheckOrder5(int variable1, int variable2, int variable3);
bool CheckOrder6(int variable1, int variable2, int variable3);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
		while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
		
		break;
	}		
	return true;
}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter the first number: ";
	cin>>variableOne;
	
	cout<<"Enter the two number: ";
	cin>>variableTwo;
	
	cout<<"Enter the third number:";
	cin>>variableThree;
	
	if (variableOne < 0){
		cout<<"Variable one not valid\r\n";
		return Error;
	}
	
	if (variableTwo < 0){
		cout<<"Variable two invalid\r\n";
		return Error;	
	}

	if (variableThree < 0){
		cout<<"Variable three invalid\r\n";
		return Error;	
	}

	if ((variableOne == variableTwo)||(variableTwo == variableThree)||(variableOne == variableThree)){
		cout<<"Variable repeated invalid\r\n";
		return Error;	
	}
	return Success;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	
	if ( CheckOrder1(variableOne,variableTwo,variableThree)){
		cout<<"the order from highest to lowest is:"<<variableOne<<" "<<variableTwo<<" "<<variableThree<<"\r\n";
 	}else	if ( CheckOrder2(variableOne,variableTwo,variableThree)){
 		cout<<"the order from highest to lowest is:"<<variableOne<<" "<<variableThree<<" "<<variableTwo<<"\r\n";
	}else	if ( CheckOrder3(variableOne,variableTwo,variableThree)){
	 	cout<<"the order from highest to lowest is:"<<variableTwo<<" "<<variableOne<<" "<<variableThree<<"\r\n";
	}else	if ( CheckOrder4(variableOne,variableTwo,variableThree)){
		cout<<"the order from highest to lowest is:"<<variableTwo<<" "<<variableThree<<" "<<variableOne<<"\r\n";	
	}else	if ( CheckOrder5(variableOne,variableTwo,variableThree)){
		cout<<"the order from highest to lowest is:"<<variableThree<<" "<<variableOne<<" "<<variableTwo<<"\r\n";	
	}else	if ( CheckOrder6(variableOne,variableTwo,variableThree)){
		cout<<"the order from highest to lowest is:"<<variableThree<<" "<<variableTwo<<" "<<variableOne<<"\r\n";
	}
		return true;
}

//=====================================================================================================

bool CheckOrder1(int variable1, int variable2, int variable3){
	if ( variable1 > variable2 && variable2 > variable3) {
		return true;	
	}else{
		return false;
	}
}
//=====================================================================================================

bool CheckOrder2(int variable1, int variable2, int variable3){
	if ( variable1 > variable3 && variable3 > variable2) {
		return true;
	}else{
		return false;
	}
}
//=====================================================================================================

bool CheckOrder3(int variable1, int variable2, int variable3){
	if (variable2 > variable1 && variable1 > variable3) {
		return true;	
	}else{
		return false;
	}
}
//=====================================================================================================

bool CheckOrder4(int variable1, int variable2, int variable3){
	if (variable2 > variable3 && variable3 > variable1)	{
		return true;		
	}else{
		return false;
	}
}
//=====================================================================================================

bool CheckOrder5(int variable1, int variable2, int variable3){
	if (variable3 > variable1 && variable1 > variable2) {
		return true;	
	}else{
		return false;
	}
}
//=====================================================================================================

bool CheckOrder6(int variable1, int variable2, int variable3){
	if (variable3 > variable2 && variable2 > variable1) {
		return true;	
	}
}
//=====================================================================================================

