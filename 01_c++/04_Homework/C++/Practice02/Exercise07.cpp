/**
 * @file CondicionalesExercise07.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 07:
 		7.- Una compa��a de alquiler de autos emite la factura de sus clientes teniendo en cuenta la distancia recorrida, si la distancia no rebasa los 300 km., se cobra una tarifa fija de S/.250, si la distancia recorrida es mayor a 300 km. y hasta 1000 km. Se cobra la tarifa fija m�s el exceso de kil�metros a raz�n de S/.30 por km. y si la distancia recorrida es mayor a 1000 km.,  la compa��a cobra la tarifa fija m�s los kms. recorridos entre 300 y 1000 a raz�n de S/. 30, y S/.20 para las distancias mayores de 1000 km. Calcular el monto que pagar� un cliente.
 * @version 1.0
 * @date 07.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalParamDistanceRango1 = 300;
int globalParamDistanceRango2 = 1000;
float traveledDistance = 0.0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool ShowResults();
float CalculatedAmountPay(float distance);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
		while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
		
		break;
	}		
	return true;
}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter the traveled distance: ";
	cin>>traveledDistance;
	
	if((traveledDistance < 0)){
		cout<<"Error traveled distance is not valid\r\n";
		return Error;
	}
	return Success;	
}
//=====================================================================================================

bool ShowResults(){
	float amountExit = 0.0;
	
	if (amountExit < 0){
		cout<<"Amount not valid\r\n";
		return false;
	}
	
	if (amountExit=CalculatedAmountPay(traveledDistance) ) {
		cout<<"The amount to be paid is:"<<amountExit<<"\r\n";;
	}
		return true	;
}
//=====================================================================================================

float CalculatedAmountPay(float distance){
	if ( globalParamDistanceRango1 >= distance) {
		return 250;		
	}else	if ( globalParamDistanceRango1 < distance && distance <= globalParamDistanceRango2) {
		return 250 + (distance-globalParamDistanceRango1)*(30);
	}else	if ( globalParamDistanceRango2 < distance) {
		return 250 + (globalParamDistanceRango2-globalParamDistanceRango1)*(30) + (distance-globalParamDistanceRango2)*(20);	
	}
}
//=====================================================================================================

