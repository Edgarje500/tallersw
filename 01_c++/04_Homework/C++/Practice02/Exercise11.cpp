/**
 * @file CondicionalesExercise11.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 11:
 		11.- Se necesita un sistema para un supermercado, en el cual si el monto de la compra del cliente es mayor de $5000 se le har� un descuento del 30%, si es menor o igual a $5000 pero mayor que $3000 ser� del 20%, si no rebasa los $3000 pero si los $1000 la rebaja efectiva es del 10% y en caso de que no rebase los $1000 no tendr� beneficio.
 * @version 1.0
 * @date 08.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalParamRangeAmount1 = 1000;
int globalParamRangeAmount2 = 3000;
int globalParamRangeAmount3 = 5000;
float amountPurchase = 0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool ShowResults();
bool CheckDiscount(float amount);
float CalculatedDiscount(float amount);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
		while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
		
		break;
	}		
	return true;
}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter you amount of purchase: ";
	cin>>amountPurchase;

	if((amountPurchase < 0)){
		cout<<"Amount purchase is not valid\r\n";
		return Error;
	}
	return Success;	
}
//=====================================================================================================

bool ShowResults(){
	float discount = 0;
	
	if (discount < 0){
		cout<<"Discount not valid\r\n";
		return false;
	}
	
	if (CheckDiscount(amountPurchase) ) {
		cout<<"you have no discount"<<"\r\n";
	}else	if(discount=CalculatedDiscount(amountPurchase)){
		cout<<"you discount is:"<<discount<<"\r\n";
	} return true;
}
//=====================================================================================================

bool CheckDiscount(float amount){
	if ( globalParamRangeAmount1 >= amount) {
		return true;
	} else{
		return false;
	}
}
//=====================================================================================================

float CalculatedDiscount(float amount){		
	if ( (globalParamRangeAmount1 < amount) && (amount <= globalParamRangeAmount2)) {
		return (0.1)*amount;
	}else	if ( (globalParamRangeAmount2 < amount) && (amount <= globalParamRangeAmount3)) {
		return amount*(0.2);
	}else	if ( globalParamRangeAmount3 < amount ) {
		return amount*(0.3);
	}	
}
//=====================================================================================================
