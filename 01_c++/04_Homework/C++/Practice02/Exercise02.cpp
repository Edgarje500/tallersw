/**
 * @file CondicionalesExercise02.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 02:
 		2.- A un  trabajador le descuentan de su sueldo el 10% si su sueldo es menor o igual a 1000, por encima de 1000 hasta 2000 el 5% del adicional, y por encima de 2000 el 3% del adicional. Calcular el descuento y sueldo neto que recibe el trabajador dado un sueldo,
 * @version 1.0
 * @date 04.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalParamRangeSalary1 = 1000;
int globalParamRangeSalary2 = 2000;

float globalParamPercentageDiscount1 = 0.1;
float globalParamPercentageDiscount2 = 0.05;
float globalParamPercentageDiscount3 = 0.03;

float salaryInput = 0.0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool ShowResults();
float calculatedDiscountExit(float salary);
float calculatedNetIncome(float salary,float discount);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
		while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
		
		break;
	}		
	return true;
}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter you salary: ";
	cin>>salaryInput;
	
	if((salaryInput < 0)){
		cout<<"Error salary is not valid\r\n";
		return Error;
	}
	return Success;	
}
//=====================================================================================================

bool ShowResults(){
	float discountExit = 0.0;
	float netIncome = 0.0;
	
	if (discountExit < 0){
		cout<<"Discount not valid\r\n";
		return false;
	}
	
	if (netIncome < 0){
		cout<<"Net income invalid\r\n";
		return false;	
	}
	
	cout<<"============Show result============\r\n";
	
	if (discountExit=calculatedDiscountExit(salaryInput) ) {
		cout<<"The discount is:"<<discountExit<<"\r\n";
	netIncome=calculatedNetIncome(salaryInput,discountExit);
		cout<<"The net income is:"<<netIncome<<"\r\n";
	}	
		return true;	
}
//=====================================================================================================

float calculatedDiscountExit(float salary){
	if ( globalParamRangeSalary1 >= salary) {
		return salary*(globalParamPercentageDiscount1);		
	}else	if ( globalParamRangeSalary1 < salary && salary <= globalParamRangeSalary2) {
		return globalParamRangeSalary1*(globalParamPercentageDiscount1) + (salary-globalParamRangeSalary1)*(globalParamPercentageDiscount2);
	}else	if ( globalParamRangeSalary2 < salary) {
		return globalParamRangeSalary1*(globalParamPercentageDiscount1) + globalParamRangeSalary1*(globalParamPercentageDiscount2) + (salary-globalParamRangeSalary2)*(globalParamPercentageDiscount3);	
	}
}
//=====================================================================================================

float calculatedNetIncome(float salary,float discount){
	return salary-discount;
}
