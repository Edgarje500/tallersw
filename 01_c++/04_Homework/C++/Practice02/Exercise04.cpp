/**
 * @file CondicionalesExercise04.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 04:
 		4.- Dado un tiempo en minutos, calcular los d�as, horas y minutos que    le corresponden.
 * @version 1.0
 * @date 07.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
 /*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int totalMinutesInput = 0;	

int days = 0;				
int hours = 0;				
int minutes = 0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
bool Calculate();
bool ShowResults();
int calculationDays(int total);
int calculationHours(int total);
int calculationMinutes(int total);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
 	
	Run();
 	return 0;	
 }
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (Calculate() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
	
		break;
	}
			
	return true;
}
//=====================================================================================================

	//Data entry in the terminal
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
 	cout<<"Calculate the number total of minutes in days ,hours ,minutes \r\n";
 	cout<<"\tEnter the number of total minutes:";
 	cin>>totalMinutesInput;
 
 	if (totalMinutesInput < 0){
		cout<<"Total minutes not valid\r\n";
		return Error;
	}
		return Success;
}
//=====================================================================================================

	//Operation of  the variables
bool Calculate(){
	days = calculationDays(totalMinutesInput);
	hours = calculationHours(totalMinutesInput);
	minutes = calculationMinutes(totalMinutesInput);
	
	if (days < 0){
		cout<<"Days not valid\r\n";
		return false;
	}
	
	if (hours < 0){
		cout<<"Hours invalid\r\n";
		return false;
	}
	
	if (minutes < 0){
		cout<<"The minutes is invalid\r\n";
		return false;
	}
		return true;
}
//=====================================================================================================

	//Show the calculation of the terminal
bool ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
 	cout<<"\tThe number of days is:"<<days<<"\r\n";
 	cout<<"\tThe number of hours is:"<<hours<<"\r\n";
 	cout<<"\tThe number of minutes is:"<<minutes<<"\r\n";
 	return true;
}
//=====================================================================================================

int calculationDays(int total){	
	return total / 1440;
}
//=====================================================================================================

int calculationHours(int total){
	total = total % 1440;
	return	total / 60;		
}
//=====================================================================================================

int calculationMinutes(int total){
	total = total % 60;
	return	total ;
}
//=====================================================================================================




