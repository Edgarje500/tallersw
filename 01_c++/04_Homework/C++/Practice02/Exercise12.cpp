/**
 * @file CondicionalesExercise12.cpp
 * @author Edgar Qui�ones (edgar06022002@gmail.com)
 * @brief exercise 12:
 		12.- Calcular la utilidad que un trabajador recibe en el reparto anual de utilidades si este se le asigna como un porcentaje de su salario mensual que depende de su antig�edad en la empresa de acuerdo con la sig. tabla:
		Tiempo						Utilidad
	Menos de 1 a�o						5 % del salario
	1 a�o o mas y menos de 2 a�os				7% del salario
	2 a�os o mas y menos de 5 a�os				10% del salario
	5 a�os o mas y menos de 10 a�os				15% del salario
	10 a�os o mas							20% del salario

 * @version 1.0
 * @date 08.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalParamSalaryUtilityRange1 = 12;
int globalParamSalaryUtilityRange2 = 24;
int globalParamSalaryUtilityRange3 = 60;
int globalParamSalaryUtilityRange4 = 120;
int monthsInput = 0;
float salaryMonthly = 0.0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool ShowResults();
float CalculatedUtility(int months, float salary);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
		while (true){
		if (CollectData() == Error){
			cout<<"Error typing the code, re-enter the code\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Entered code is invalid, re-enter the code\r\n";
			continue;
		}
		
		break;
	}		
	return true;
}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter you months working: ";
	cin>>monthsInput;
	cout<<"Enter you salary monthly: ";
	cin>>salaryMonthly;
	
	if((monthsInput < 0)){
		cout<<"Months is not valid\r\n";
		return Error;
	}
	
	if((salaryMonthly < 0)){
		cout<<"Salary of the months is not valid\r\n";
		return Error;
	}
	return Success;
}
//=====================================================================================================

bool ShowResults(){
	float utility = 0;

	if (utility < 0){
		cout<<"Utility not valid\r\n";
		return false;
	}
	
	if (utility=CalculatedUtility(monthsInput,salaryMonthly) ) {
		cout<<"The utility is:"<<utility<<"\r\n";
	}
		return true;
}
//=====================================================================================================

float CalculatedUtility(int months, float salary){
	if ( globalParamSalaryUtilityRange1 >= months) {
		return salary*(0.05);		
	}else	if ( globalParamSalaryUtilityRange1 < months && months <= globalParamSalaryUtilityRange2) {
		return salary*(0.07);
	}else	if ( globalParamSalaryUtilityRange2 < months && months <= globalParamSalaryUtilityRange3) {
		return salary*(0.1);	
	}else	if ( globalParamSalaryUtilityRange3 < months && months <= globalParamSalaryUtilityRange4) {
		return salary*(0.15);	
	}else	if ( globalParamSalaryUtilityRange4 < months ) {
		return salary*(0.2);	
	}
}
//=====================================================================================================

